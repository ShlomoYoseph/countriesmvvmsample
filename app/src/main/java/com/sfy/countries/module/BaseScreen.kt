package com.sfy.countries.module

import androidx.annotation.LayoutRes

interface BaseScreen<T : BaseViewModel> {

    @LayoutRes
    fun getLayoutResource(): Int

    fun supplyViewModel(): T

    fun setupViews()

    fun bindViewModel()

}