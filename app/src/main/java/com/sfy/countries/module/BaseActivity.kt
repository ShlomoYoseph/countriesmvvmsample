package com.sfy.countries.module

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseActivity<T : BaseViewModel> : DaggerAppCompatActivity(),BaseScreen<T> {

    private val compositeDisposable = CompositeDisposable()

    val viewModel: T by lazy {
        supplyViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResource())
        setupViews()

    }

    override fun onStart() {
        super.onStart()
        bindViewModel()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    @CallSuper
    override fun bindViewModel() {
    }

    fun Disposable.addDisposable() {
        compositeDisposable.add(this)
    }

}

