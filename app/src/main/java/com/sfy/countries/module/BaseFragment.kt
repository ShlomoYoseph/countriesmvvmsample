package com.sfy.countries.module

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseFragment<T : BaseViewModel> : DaggerFragment(), BaseScreen<T> {

    val baseActivity by lazy { activity as? BaseActivity<*> }

    private val compositeDisposable = CompositeDisposable()
    var rootView : View? = null

    val viewModel: T by lazy {
        supplyViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        if (view != null) {
            if (view!!.parent != null) {
                (view!!.parent as ViewGroup).removeView(view)
            }
            return view
        }

        return inflater.inflate(getLayoutResource(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        bindViewModel()
    }

    override fun onDestroyView() {
        compositeDisposable.clear()
        super.onDestroyView()
    }

    @CallSuper
    override fun bindViewModel() {
        val baseActivity = activity as? BaseActivity<*> ?: return
        if (viewModel == baseActivity.viewModel) return
    }


    fun Disposable.addDisposable() {
        compositeDisposable.add(this)
    }

}