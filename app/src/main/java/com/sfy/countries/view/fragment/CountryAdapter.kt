package com.sfy.countries.view.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sfy.countries.R
import com.sfy.countries.data.Country
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject


class CountryAdapter : RecyclerView.Adapter<CountryAdapter.ViewHolder>() {

    private var list = mutableListOf<Country>()
    val onClickCountryItem = BehaviorSubject.create<Country>()

    fun setList(countrylist: List<Country>) {
        if (list.size > 0) {
            list.clear()
            notifyDataSetChanged()
        }
        list.addAll(countrylist)
        notifyItemRangeChanged(0, list.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.country_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

        private val name = itemview.findViewById<TextView>(R.id.name)
        private val nativeName = itemview.findViewById<TextView>(R.id.native_name)

        init {
            itemview.setOnClickListener{ onClickCountryItem.onNext(list[adapterPosition]) }
        }

        fun bind() {
            val item = list[adapterPosition]
            name.text = item.name
            nativeName.text = item.nativeName

        }
    }

    fun sortListByArea(ascending:Boolean){
        val countryList = mutableListOf<Country>()
        countryList.addAll(list)

        if(ascending) countryList.sortBy { country -> country.area }
        else countryList.sortByDescending {  country -> country.area }
        setList(countryList)
    }

    fun sortListByName(ascending:Boolean){
        var countryList = mutableListOf<Country>()
        countryList.addAll(list)

        if(ascending) countryList.sortBy { country -> country.name }
        else countryList.sortByDescending {  country -> country.name }
        setList(countryList)
        countryList.clear()
    }
}