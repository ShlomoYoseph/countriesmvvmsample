package com.sfy.countries.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sfy.countries.api.CountriesRepository
import com.sfy.countries.view.fragment.CountryListViewModel
import com.sfy.countries.view.fragment.DetailsViewModel
import javax.inject.Inject

class MainViewModelFactory
@Inject
constructor(private val repository: CountriesRepository) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) ->
                MainViewModel()
            modelClass.isAssignableFrom(CountryListViewModel::class.java) ->
                CountryListViewModel(repository)
            modelClass.isAssignableFrom(DetailsViewModel::class.java) ->
                DetailsViewModel(repository)
            else -> throw Throwable("Unknown ViewModel class")
        } as T
    }

}

