package com.sfy.countries.view.fragment

import com.sfy.countries.api.CountriesRepository
import com.sfy.countries.data.Country
import com.sfy.countries.module.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class CountryListViewModel(private val repository: CountriesRepository) : BaseViewModel() {

    var disposable : CompositeDisposable? = CompositeDisposable()

    val showList:PublishSubject<List<Country>> = PublishSubject.create()
    val showLoader:PublishSubject<Boolean> = PublishSubject.create()
    val goToDetailFragment:PublishSubject<Boolean> = PublishSubject.create()
    val sortByName:PublishSubject<Boolean> = PublishSubject.create()
    val sortByArea:PublishSubject<Boolean> = PublishSubject.create()

    fun fetchCountriesList(){
        disposable!!.add( repository.getAllCountries()
            .doOnSubscribe { showLoader.onNext(true) }
            .doFinally { showLoader.onNext(false) }
            .subscribe({ showList.onNext(it) }, {showList.onError(it)}))
    }

    fun sortByNameAscending(ascending: Boolean){
        sortByName.onNext(ascending)
    }

    fun sortByAreaAscending(ascending: Boolean){
        sortByArea.onNext(ascending)
    }

    fun navigateToDetail(){
        goToDetailFragment.onNext(true)
    }

    override fun onCleared() {
        super.onCleared()
        if(disposable!=null) {
            disposable!!.clear()
            disposable = null
        }
    }
}
