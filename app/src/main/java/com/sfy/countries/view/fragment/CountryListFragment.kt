package com.sfy.countries.view.fragment

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sfy.countries.R
import com.sfy.countries.module.BaseFragment
import com.sfy.countries.view.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_country_list.*
import javax.inject.Inject


class CountryListFragment: BaseFragment<CountryListViewModel>(){

    private val mainActivity by lazy { activity as? MainActivity }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var countryAdapter:CountryAdapter? = null

    override fun getLayoutResource() = R.layout.fragment_country_list

    override fun supplyViewModel() =  ViewModelProviders.of(activity!!, viewModelFactory).get(CountryListViewModel::class.java)

    override fun setupViews() {
        retainInstance = true
        setHasOptionsMenu(true)
        mainActivity?.toolbar?.title = getString(R.string.app_name)
        initRecyclerview()
        viewModel.fetchCountriesList()
    }

    override fun bindViewModel() {
        super.bindViewModel()

        viewModel.showLoader.subscribe{
            println("bindViewModel showLoader")
            loader.visibility = if(it) View.VISIBLE else View.GONE}
            .addDisposable()

        viewModel.showList.subscribe {
            println("bindViewModel getCountriesList")
            country_list_recyclerview.recycledViewPool.clear()
            countryAdapter!!.setList(it)
            mainActivity!!.viewModel.setlist(it)
        }.addDisposable()

        countryAdapter!!.onClickCountryItem.subscribe{
            println("bindViewModel onClickCountryItem")
            mainActivity?.viewModel?.selectedItem(it)
            viewModel.navigateToDetail()
        }.addDisposable()

        viewModel.sortByName.subscribe { countryAdapter?.sortListByName(it) }.addDisposable()
        viewModel.sortByArea.subscribe { countryAdapter?.sortListByArea(it) }.addDisposable()

        viewModel.goToDetailFragment.subscribe{
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, DetailFragment(),
                DetailFragment::class.simpleName).addToBackStack(null).commit()
        }
            .addDisposable()
    }

    private fun initRecyclerview() {

        country_list_recyclerview.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
            countryAdapter = CountryAdapter()
            adapter = countryAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sort_name_ascending -> {
                viewModel.sortByNameAscending(true)
                return true
            }
            R.id.sort_name_descending -> {
                viewModel.sortByNameAscending(false)
                return true
            }

            R.id.sort_area_ascending -> {
                viewModel.sortByAreaAscending(true)
                return true
            }
            R.id.sort_area_descending -> {
                viewModel.sortByAreaAscending(false)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}