package com.sfy.countries.view

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.sfy.countries.R
import com.sfy.countries.module.BaseActivity
import com.sfy.countries.view.fragment.CountryListFragment
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity<MainViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun getLayoutResource() = R.layout.activity_main

    override fun supplyViewModel() = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

    override fun setupViews() {
        setSupportActionBar(toolbar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, CountryListFragment(),
            CountryListFragment::class.simpleName).commit()
    }
}
