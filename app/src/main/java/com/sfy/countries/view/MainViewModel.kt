package com.sfy.countries.view

import com.sfy.countries.data.Country
import com.sfy.countries.module.BaseViewModel

class MainViewModel : BaseViewModel() {
    var country:Country? = null
    var list : List<Country> = emptyList()

    fun selectedItem(country: Country) {
        this.country = country
    }

    fun setlist(countries: List<Country>) {
        this.list = countries
    }

}
