package com.sfy.countries.view.fragment

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.sfy.countries.R
import com.sfy.countries.data.Country
import com.sfy.countries.module.BaseFragment
import com.sfy.countries.view.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject


class DetailFragment: BaseFragment<DetailsViewModel>(){

    val mainActivity by lazy { activity as? MainActivity }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var borderCountriesAdapter:BorderCountriesAdapter? = null
    var country : Country? = null

    override fun getLayoutResource() = R.layout.fragment_detail

    override fun supplyViewModel() =  ViewModelProviders.of(activity!!, viewModelFactory).get(DetailsViewModel::class.java)

    override fun setupViews() {
        country = mainActivity!!.viewModel.country
        mainActivity?.toolbar?.title = country!!.name

        initRecyclerview()
        val borderingCountries = country!!.borders
        viewModel.getBorderingCountries(borderingCountries, mainActivity!!.viewModel.list)
    }

    override fun bindViewModel() {
        super.bindViewModel()

        viewModel.showEmptyView.subscribe{ empty_view.visibility = if(it) View.VISIBLE else View.INVISIBLE }
            .addDisposable()

        viewModel.showBorderingList.subscribe{borderCountriesAdapter!!.setlist(it)}
            .addDisposable()
    }

    private fun initRecyclerview() {
        border_list_recyclerview.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.HORIZONTAL))
            borderCountriesAdapter = BorderCountriesAdapter()
            adapter = borderCountriesAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
    }
}