package com.sfy.countries.view.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sfy.countries.R
import com.sfy.countries.data.Country


class BorderCountriesAdapter : RecyclerView.Adapter<BorderCountriesAdapter.ViewHolder>() {

    private var list = mutableListOf<Country>()

    fun setlist(countrylist: List<Country>){
        countrylist.forEach {
            list.add(it)
            notifyItemInserted(list.size-1)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.country_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = list.size

    fun getItem(position: Int): Country {
        return list[position]
    }

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

        private val name = itemview.findViewById<TextView>(R.id.name)
        private val nativeName = itemview.findViewById<TextView>(R.id.native_name)

        fun bind() {
            val item = list[adapterPosition]
            name.text = item.name
            nativeName.text = item.nativeName

        }
    }
}