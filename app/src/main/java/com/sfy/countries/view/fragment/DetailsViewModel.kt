package com.sfy.countries.view.fragment

import com.sfy.countries.api.CountriesRepository
import com.sfy.countries.data.Country
import com.sfy.countries.module.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class DetailsViewModel(private val repository: CountriesRepository) : BaseViewModel() {

    val disposable : CompositeDisposable? = CompositeDisposable()

    val showBorderingList: PublishSubject<List<Country>> = PublishSubject.create()
    val showEmptyView:PublishSubject<Boolean> = PublishSubject.create()

    fun getBorderingCountries(borderingCountries: List<String>?, list: List<Country>) {
        if(borderingCountries!!.isEmpty()){
            showEmptyView.onNext(true)
        } else {
            disposable!!.add((repository.getBorderCountries(borderingCountries, list))
                .subscribe({ showBorderingList.onNext(it)
                    showEmptyView.onNext(false)}, { showBorderingList.onError(it) })
            )
        }
    }
}
