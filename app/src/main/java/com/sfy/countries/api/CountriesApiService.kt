package com.sfy.countries.api

import com.sfy.countries.data.Country
import io.reactivex.Single
import retrofit2.http.GET


interface CountriesApiService {

    @GET("all?fields=name;nativeName;alpha3Code;area;borders")
    fun getAllCountries(): Single<List<Country>>
}