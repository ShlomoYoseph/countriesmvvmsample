package com.sfy.countries.api

import com.sfy.countries.data.Country
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton


@Singleton
class CountriesRepository
constructor(private val apiService: CountriesApiService) {

    fun getAllCountries(): Single<List<Country>> {
        return apiService.getAllCountries().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getBorderCountries(bordersList: List<String>?, list: List<Country>):Single<List<Country>>{
        val contries = ArrayList<Country>()
        bordersList?.forEachIndexed { _, s ->
            val item = list.find { it.alpha3Code.equals(s) }
            if(item!=null){
                contries.add(item)
            }
        }

        return Single.just(contries.toList()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}