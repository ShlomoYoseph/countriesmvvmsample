package com.sfy.countries.api


object ApiConfig {
    const val BASE_URL = "https://restcountries.eu/rest/v2/"
}