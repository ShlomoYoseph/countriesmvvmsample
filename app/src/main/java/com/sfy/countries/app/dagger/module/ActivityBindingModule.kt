package com.sfy.countries.app.dagger.module


import com.sfy.countries.view.MainActivity
import com.sfy.countries.view.MainModule
import com.sfy.countries.app.dagger.scope.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [(MainModule::class)])
    internal abstract fun mainActivity(): MainActivity

}
