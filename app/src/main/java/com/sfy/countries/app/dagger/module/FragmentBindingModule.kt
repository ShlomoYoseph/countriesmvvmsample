package com.sfy.countries.app.dagger.module


import com.sfy.countries.view.MainModule
import com.sfy.countries.view.fragment.CountryListFragment
import com.sfy.countries.app.dagger.scope.FragmentScoped
import com.sfy.countries.view.fragment.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = [(MainModule::class)])
    internal abstract fun countryListFragment(): CountryListFragment

    @FragmentScoped
    @ContributesAndroidInjector(modules = [(MainModule::class)])
    internal abstract fun detailFragment(): DetailFragment

}

