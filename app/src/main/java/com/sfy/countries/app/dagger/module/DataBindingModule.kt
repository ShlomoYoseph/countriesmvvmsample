package com.sfy.countries.app.dagger.module


import com.sfy.countries.api.CountriesApiService
import com.sfy.countries.api.CountriesRepository

import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
internal class DataBindingModule {

    @Provides
    @Singleton
    fun provideContriesRepository(remoteDataSource: CountriesApiService): CountriesRepository {
        return CountriesRepository(remoteDataSource)
    }
}
