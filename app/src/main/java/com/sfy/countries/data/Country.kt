package com.sfy.countries.data
import com.google.gson.annotations.SerializedName

data class Country (

	@SerializedName("name") val name : String?,
	@SerializedName("alpha3Code") val alpha3Code : String?,
	@SerializedName("area") val area : Double?,
	@SerializedName("borders") val borders : List<String>?,
	@SerializedName("nativeName") val nativeName : String?
)